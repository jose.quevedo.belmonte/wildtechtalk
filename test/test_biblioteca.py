import pandas as pd
import pymysql
import src.biblioteca as biblioteca


def test_get_db_connect():
    host = "localhost"
    user = "root"
    passwd = "secret"
    database = "biblioteca"
    assert type(biblioteca.get_db_connect(host, user, passwd, database)) == pymysql.connections.Connection


def test_get_data():
    table = "loans"
    host = "localhost"
    user = "root"
    passwd = "secret"
    database = "biblioteca"
    df_mock = {
        'userId': [1, 2],
        'firstName': ['Jose', 'Albert'],
        'lastName': ['Quevedo', 'Worker'],
        'lendBooks': [3, 5]
    }

    dbcon = biblioteca.get_db_connect(host, user, passwd, database)
    assert type(biblioteca.get_data(table, dbcon)) == pd.core.frame.DataFrame
    #assert biblioteca.get_data(table, dbcon).columns.all() == pd.DataFrame(df_mock).columns.all()


def test_get_total_borrowed_books():
    df_mock = {
        'userId': [3, 4, 5],
        'firstName': ['Armando', 'Peter', 'Jake'],
        'lastName': ['Guerra', 'Parker', 'TheDog'],
        'lendBooks': [4, 2, 9]
    }
    assert biblioteca.get_total_borrowed_books(pd.DataFrame(df_mock)) == 15


def test_calculate_orchestrator(mocker):
    df_mock = {
        'userId': [3, 4, 5],
        'firstName': ['Armando', 'Peter', 'Jake'],
        'lastName': ['Guerra', 'Parker', 'TheDog'],
        'lendBooks': [4, 2, 9]
    }
    mocker.patch(
        'src.biblioteca.get_db_connect',
        return_value=pymysql.connections.Connection
    )
    mocker.patch(
        'src.biblioteca.get_data',
        return_value=pd.DataFrame(df_mock)
    )
    expected = 15
    actual = biblioteca.calculate_orchestrator()
    assert expected == actual


