class Person:
    def __init__(self, name, income, rent):
        self.name = name
        self.income = income
        self.rent = rent

    def calculate_savings(self):
        return self.income - self.rent

    def get_class(self):
        x = self.calculate_savings()
        if x > 0:
            return "rich"
        elif x < 0:
            return "poor"
        else:
            return "normal"
