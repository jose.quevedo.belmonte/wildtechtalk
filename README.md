# wildTechTalk

## Desplegar sonarqube

Para desplegar vuestro codigo en sonar desplegar el contenido de sonar-project.properties en un terminal dando de alta previamente vuestro token en la web accediendo a 

```
http://localhost:9000/
```

## Comprobar coverance

Para obtener la covertura de nuestros test ejecutar el siguiente comando desde la ruta raiz de nuestro proyecto
```
pytest --cov-report term --cov=.
```

## Comprobar fixtures

Para poder echar un vistazo a las fixtures que hemos desarrollado ejecutar

```
pytest --fixtures
```