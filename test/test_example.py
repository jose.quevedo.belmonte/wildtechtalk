import pytest

from src.example import Person


@pytest.fixture
def rich_person():
    '''Returns a Person instance with a balance more than 0'''
    return Person("Alberto", 1000000, 8000)


@pytest.fixture
def normal_person():
    '''Returns a Person instance with a balance equals 0'''
    return Person("Jake", 2000, 2000)


@pytest.fixture
def ruined_person():
    '''Returns a Person instance with a balance less than 0'''
    return Person("Pepe", 10, 200)


def test_calculate_savings(ruined_person):
    assert ruined_person.calculate_savings() < 0


def test_calculate_savings(normal_person):
    assert normal_person.calculate_savings() == 0


def test_calculate_savings(rich_person):
    assert rich_person.calculate_savings() > 0


def test_get_class(rich_person):
    assert rich_person.get_class() == "rich"


def test_get_class(normal):
    assert normal.get_class() == "normal"


def test_get_class(ruined_person):
    assert ruined_person.get_class() == "poor"
