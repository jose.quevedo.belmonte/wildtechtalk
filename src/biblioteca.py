import pandas as pd
import pymysql


def get_db_connect(host, user, pwd, db):
    return pymysql.connect(host=host, user=user, passwd=pwd, database=db)


def get_data(table, dbcon):
    try:
        sql_query = pd.read_sql_query(
            '''select
              *
              from ''' + table, dbcon)

        df = pd.DataFrame(sql_query)
    except:
        print("Error: unable to convert the data")
    dbcon.close()
    return df


def get_total_borrowed_books(df):
    return df["lendBooks"].sum()





def calculate_orchestrator():
    dbcon = get_db_connect("localhost", "root", "secret", "biblioteca")
    df_loans = get_data("loans", dbcon)
    return get_total_borrowed_books(df_loans)


if __name__ == '__main__':
    print(calculate_orchestrator())
