# wildTechTalk

## Contenido de este docker compose

Para levantar estos ejemplos ir a la ruta donde tengais el fichero docker-compose.yml y ejecutar

```
docker-compose up
```

En caso de querer detener el servicio ejecutar el comando:

```
docker-compose down
```

Si quereis ver el estado de vuestros docker, abrir un nuevo terminal y ejecutar el siguiente comando

```
docker-compose ps -a
```

Si quereis acceder de manera iterativa a cualquiera de estas imagenes utilizar el comando

```
docker -exec -it <nombre_del_docker> /bin/bash
```

### mysql
Base de datos del ejemplo de biblioteca para conectaros desde un gestor de Base de datos

Credenciales
```
host: localhost
port: 3306
database: biblioteca
user: root
pass: secret
```


DDL
```
-- biblioteca.loans definition

CREATE TABLE `loans` (
  `userId` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `lendBooks` int NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### sonarqube
Sonar utilizado para la visualización en la formación 

Credenciales

```
user: admin
pass: admin
```